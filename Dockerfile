FROM python

RUN git clone https://github.com/adrienbrignon/cloudflare-ddns.git

RUN pip install -r cloudflare-ddns/requirements.txt

ADD config.yml cloudflare-ddns/config.yml

ADD update-dns.sh /usr/local/bin/update-dns.sh

RUN chmod +x /usr/local/bin/update-dns.sh

RUN chmod +x cloudflare-ddns/cloudflare-ddns.py

ENTRYPOINT /usr/local/bin/update-dns.sh
