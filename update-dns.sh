#!/bin/bash

while true
do
    sed -i -r 's~%%CLOUDFLARE_API_KEY%%~'${CF_API_KEY}'~g' /cloudflare-ddns/config.yml
    sed -i -r 's~%%CLOUDFLARE_EMAIL%%~'${CF_API_EMAIL}'~g' /cloudflare-ddns/config.yml
    sed -i -r 's~%%CLOUDFLARE_DOMAIN%%~'${CF_DOMAIN}'~g' /cloudflare-ddns/config.yml
    sed -i -r 's~%%CLOUDFLARE_SUBDOMAIN%%~'${CF_SUBDOMAIN}'~g' /cloudflare-ddns/config.yml
    cp /cloudflare-ddns/config.yml /cloudflare-ddns/zones/${CF_DOMAIN}.yml
    timeout 60 /cloudflare-ddns/cloudflare-ddns.py -z ${CF_DOMAIN}
    sleep 300
done
